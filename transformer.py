import pandas as pd
from rdflib import Graph, URIRef, Literal, Namespace
from rdflib.namespace import RDF, RDFS, XSD
import os
import sys

# Check if a file path is provided
if len(sys.argv) != 2:
    print("Could not run the script.\nUsage: python transformer.py path/to/dataset.csv")
    sys.exit(1)

# Input file path
file_path = sys.argv[1]
print(f"Reading dataset from: {file_path}")

# Read the dataset
data = pd.read_csv(file_path)
print(f"Dataset loaded successfully. Number of records: {len(data)}\nBeginning RDF conversion...")

# Create a graph and define namespace
g = Graph()
SAREF = Namespace('https://saref.etsi.org/')
EXAMPLE = Namespace('http://example.org/')

# Bind namespace prefixes
g.bind('saref', SAREF)
g.bind("ex", EXAMPLE)

# Create RDF URI's related to the data
building = URIRef("http://example.org/Building")
industrial = URIRef("http://example.org/IndustrialBuilding")
public = URIRef("http://example.org/PublicBuilding")
residential = URIRef("http://example.org/ResidentialBuilding")
measured_power = URIRef("http://example.org/MeasuredPower")
metering_function = URIRef("http://example.org/MeteringFunction")

# Define building subtypes
g.add((industrial, RDFS.subClassOf, building))
g.add((public, RDFS.subClassOf, building))
g.add((residential, RDFS.subClassOf, building))
g.add((metering_function, SAREF.hasMeterReadingType, measured_power))

# Triplification
print("Starting triplification...")
for col_name in data.columns:
    if col_name in ["utc_timestamp", "cet_cest_timestamp", "interpolated"]:
        continue

    # Building type allocation
    building_type = col_name.split('_')[2]
    building_uri = URIRef(f"http://example.org/{building_type}")

    if "industrial" in col_name:
        g.add((building_uri, RDF.type, industrial))
    elif "public" in col_name:
        g.add((building_uri, RDF.type, public))
    elif "residential" in col_name:
        g.add((building_uri, RDF.type, residential))
    else:
        print(f"Building type not recognized for column {col_name}")

    # Add meters to buildings
    meter = URIRef(f"http://example.org/{col_name}")

    g.add((meter, RDF.type, SAREF.Meter))
    g.add((meter, SAREF.measuresProperty, measured_power))
    g.add((meter, SAREF.hasFunction, metering_function))
    g.add((meter, RDFS.label, Literal(col_name.replace("_", " "), lang="en")))
    g.add((meter, EXAMPLE.locatedIn, building_uri))

    # Loop to add measurement data to meters
    print(f"Processing measurements for {col_name}...")
    for i, row in data.iterrows():
        time = row["utc_timestamp"]

        value = row[col_name]
        if pd.isnull(value):
            continue

        interpolated = row["interpolated"]

        if isinstance(interpolated, str):
            is_interpolated = col_name in interpolated
        else:
            is_interpolated = False

        measurement = URIRef(f"http://example.org/{col_name}_{i}")
        g.add((measurement, RDF.type, SAREF.Measurement))
        g.add((measurement, SAREF.hasValue, Literal(value, datatype=XSD.decimal)))
        g.add((measurement, SAREF.hasTimestamp, Literal(time, datatype=XSD.dateTime)))
        g.add((measurement, SAREF.isMeasuredIn, Literal("kW")))
        g.add((measurement, SAREF.relatesToProperty, measured_power))
        g.add((meter, SAREF.makesMeasurement, measurement))
        g.add((metering_function, SAREF.hasMeterReading, measurement))
        g.add((measurement, EXAMPLE.isInterpolated, Literal(is_interpolated, datatype=XSD.boolean)))

# Serialize the resulting RDF graph in Turtle format
print("This will take a moment. Serializing graph to Turtle...")
output_turtle_path = os.path.join(os.path.dirname(__file__), 'graph.ttl')
g.serialize(destination=output_turtle_path, format='turtle')
print(f"Process complete: RDF graph has been serialized to {output_turtle_path}.")